package com.example.myapplication;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button[] answersButton = new Button[7];
    Button btnHint, btnRestart;
    int posToGone;
    Random randomVariable = new Random();
    private int correctPosition;
    private boolean correctAnswerFound;
    private int scoreCount;
    private int levelCount;
    private int hintCount = 3;
    private int restartCount = 3;
    private boolean isFirstClicked = true;

    private static void shuffleArray(int[] ar) {
        Random rnd = new Random();
        for (int i = ar.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnHint = findViewById(R.id.btnHint);
        btnRestart = findViewById(R.id.btnRestart);


        for (int i = 0; i < 7; i++) {

            String buttonID = "btn" + i;
            int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
            answersButton[i] = findViewById(resID);
            answersButton[i].setOnClickListener(this);

        }

        setAnswer();


        btnHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isFirstClicked) {
                    hintCount--;
                    btnHint.setText("" + hintCount);
                }
                isFirstClicked = false;
                if (posToGone == 0) {
                    answersButton[4].setVisibility(View.INVISIBLE);
                } else if (posToGone == 1) {
                    answersButton[5].setVisibility(View.INVISIBLE);
                } else {
                    answersButton[6].setVisibility(View.INVISIBLE);
                }

            }


        });

        btnRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFirstClicked) {
                    restartCount--;
                    btnRestart.setText("" + restartCount);
                    next();
                    btnHint.setEnabled(false);
                }

                isFirstClicked = false;
            }
        });

    }

    public int[] randomNumber(int size, int maxN) {
        int[] randomInts = new int[size];

        for (int i = 0; i < size; i++) {
            randomInts[i] = (int) (Math.random() * maxN);
        }

        return randomInts;
    }

    @SuppressLint("SetTextI18n")
    private void setAnswer() {
        int[] a;
        int correctAnswer;
        int answer2 = 0;
        int answer3 = 0;
        int b;
        int c;
        int count;

        String level = getIntent().getStringExtra("level");
        if (level.equals("hard")) {
            a = randomNumber(2, 15);
            count = 6;

        } else {
            a = randomNumber(2, 8);
            count = 4;
        }

        int randomCheck = randomVariable.nextInt(count);

        if (randomCheck <= 1) {
            b = a[0] + a[1];
            c = b + a[1];
            correctAnswer = c + a[1];
        } else if (randomCheck <= 3) {
            b = a[0] * a[1];
            c = b * a[1];
            correctAnswer = c * a[1];
        } else {
            b = a[0] - a[1];
            c = b - a[1];
            correctAnswer = c - a[1];
        }
        answersButton[0].setText("" + a[0]);
        answersButton[1].setText("" + b);
        answersButton[2].setText("" + c);


        answer2 = (int) (Math.random() * correctAnswer);
        answer3 = correctAnswer + 1 + ((int) (Math.random() * 10));

        if (answer2 == correctAnswer) {
            answer2--;
        }

        int[] answers = {answer2, answer3, correctAnswer};
        shuffleArray(answers);


        int[] positionsToGone = new int[2];
        int goneI = 0;

        for (int i = 0; i < answers.length; i++) {
            if (answers[i] == correctAnswer) {
                correctPosition = i;
            } else {
                positionsToGone[goneI] = i;
                goneI++;
            }
        }

        shuffleArray(positionsToGone);
        posToGone = positionsToGone[0];


        // posToGone=getPosToGone[1];

        // if (goneI==correctPosition){
        //   goneI++;
        //}


        //  if (correctPosition == 0) {
        //    goneI++;
        //  posToGone = positionsToGone[goneI];
        //} else {
        //  posToGone = positionsToGone[0];
        //}


        answersButton[4].setText("" + answers[0]);
        answersButton[5].setText("" + answers[1]);
        answersButton[6].setText("" + answers[2]);


    }

    private void checkAnswer(Button button) {
        if (correctAnswerFound) {
            return;
        }

        int currentPosition;
        switch (button.getId()) {
            case R.id.btn4:
                currentPosition = 0;
                break;
            case R.id.btn5:
                currentPosition = 1;
                break;
            default:
                currentPosition = 2;
        }

        if (correctPosition == currentPosition) {
            scoreCount += 2;
            levelCount++;
            correctAnswerFound = true;
            button.setBackgroundColor(getResources().getColor(R.color.TrueAnswerColor));
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    next();
                }
            }, 1000);


        } else {
            button.setBackgroundColor(getResources().getColor(R.color.WrongAnswerColor));
            dialog();
            correctAnswerFound = true;
            Button correctButton;

            if (correctPosition == 0) {
                correctButton = answersButton[4];
            } else if (correctPosition == 1) {
                correctButton = answersButton[5];
            } else {
                correctButton = answersButton[6];
            }
            correctButton.setBackgroundColor(getResources().getColor(R.color.TrueAnswerColor));
        }


    }

    private void next() {
        btnHint.setEnabled(true);
        isFirstClicked = true;
        correctAnswerFound = false;
        restViews();
        if (isLevelFinished()) {
            dialog();
            return;
        }
        setAnswer();


    }


    @Override
    public void onClick(View v) {
        checkAnswer((Button) v);

    }


    @SuppressLint({"CutPasteId", "SetTextI18n"})
    private void dialog() {
        TextView txtScore;
        ImageView imgCover;
        ImageView imgMenu;
        ImageView imgRestart;


        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setCancelable(false);
        @SuppressLint("InflateParams") View view = getLayoutInflater().inflate(R.layout.dialog_activity, null);
        builder.setView(view);
        final AlertDialog dialog = builder.create();

        txtScore = view.findViewById(R.id.txtScore);
        imgCover = view.findViewById(R.id.imgCover);
        imgMenu = view.findViewById(R.id.imgMenu);
        imgRestart = view.findViewById(R.id.imgRestart);
        imgCover.setImageResource(R.drawable.header);

        txtScore.setText("" + scoreCount);

        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, FirstActivity.class));
                finish();


            }
        });

        imgRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                restart();
            }
        });


        dialog.show();


    }

    private void restart() {
        levelCount = 0;
        scoreCount = 0;
        hintCount = 3;
        btnHint.setText("" + hintCount);
        next();


    }

    private boolean isLevelFinished() {
        return levelCount >= 3;
    }

    private void restViews() {
        for (int i = 0; i < answersButton.length; i++) {
            answersButton[i].setText("");
            answersButton[3].setText("?");
            answersButton[i].setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                answersButton[i].setBackground(getResources().getDrawable(R.drawable.button));
            } else {
                answersButton[i].setBackgroundDrawable(getResources().getDrawable(R.drawable.button));
            }
        }

    }
}




