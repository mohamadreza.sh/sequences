package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class FirstActivity extends AppCompatActivity {
    private Button btnEasy;
    private Button btnHard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        btnEasy = findViewById(R.id.btnEasy);
        btnHard = findViewById(R.id.btnHard);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FirstActivity.this, MainActivity.class);

                if (v.getId() == R.id.btnEasy) {
                    intent.putExtra("level", "easy");
                } else {
                    intent.putExtra("level", "hard");

                }

                startActivity(intent);
                finish();


            }
        };


        btnEasy.setOnClickListener(onClickListener);
        btnHard.setOnClickListener(onClickListener);

    }
}
